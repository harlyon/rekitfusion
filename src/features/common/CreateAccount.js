import React, { Component } from 'react';

export default class CreateAccount extends Component {
  static propTypes = {};

  render() {
    return (
      <div className="wrapper">
        <div className="row">
          <div className="login">
            <h2 className="sign">Sign Up</h2>
            <input name="username" placeholder="Username" type="text" />
            <input id="pw" name="password" placeholder="Password" type="password" />
            <input name="email" placeholder="E-Mail Address" type="text" />
            <br />
            <br />
            <input class="animated" type="submit" value="Register" />
            
          </div>
        </div>
      </div>
    );
  }
}
