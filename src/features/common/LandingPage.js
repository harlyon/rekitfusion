import React, { Component } from 'react';

export default class LandingPage extends Component {
  static propTypes = {};

  render() {
    return (
      <body>
        <section class="intro">
          <row>
            <div class="col-lg-6 col-sm-12 left wrapper welcome">
              <p>
                <h1 className="text-center">ORNICO FUSION</h1>
                <br />
                <p>
                  Ornico Fusion is your media data and analytical web platform helping you make sound strategic
                  marketing decisions. <br />Breaking down the marketing silos, Ornico Fusion provides relevant
                  editorial, advertising and social media data and analysis, across a multitude of mediums. Now you can
                  easily navigate the media clutter with fully interactive dashboards to identify competitor trends,
                  brand performance, share of voice, media spend and much much more! Get the know how to grow your brand
                  now – join the Ornico Fusion community!
                </p>
              </p>
            </div>
            <div class="col-lg-6 col-sm-12 right">
              <div className="login">
                <br />
                <br />
                <br />
                <br />
                <h2 className="sign">Welcome Back</h2>
                <input name="username" placeholder="Username" type="text" required/>
                <input id="pw" name="password" placeholder="Password" type="password" required/>
                <br />
                <br />
                <input class="animated" type="submit" value="Sign In" />
                <a class="forgot" href="#">Sign up to Ornico Fusion</a>
              </div>
            </div>
          </row>
        </section>
      </body>
    );
  }
}
