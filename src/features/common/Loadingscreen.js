import React, { Component } from 'react';

export default class Loadingscreen extends Component {
  static propTypes = {};

  render() {
    return (
      <div className="wrapper">
        <div className="row">
          <div className="caption">
            <div className="cli">
              <div className="sections col-md-12">
                <span>hello</span>
                <span>client name</span>
                <span>welcome</span>
                <span>ornico</span>
                <h1>fusion</h1>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
