import React, { Component } from 'react';

export default class Navbar extends Component {
  static propTypes = {};

  render() {
    return (
      <nav className="navbar navbar-inverse">
        <div className="container-fluid">
          <div className="navbar-header">
            <button
              type="button"
              className="navbar-toggle collapsed"
              data-toggle="collapse"
              data-target="#bs-example-navbar-collapse-1"
              aria-expanded="false"
            >
              <span className="sr-only">Toggle navigation</span>
              <span className="icon-bar" />
              <span className="icon-bar" />
              <span className="icon-bar" />
            </button>
            <a className="navbar-brand" href="/">
             
            </a>
          </div>

          <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul className="nav navbar-nav">
              <li>
                <a href="/">Home</a>
              </li>
              <li>
                <a href="nav-link">Advertising</a>
              </li>
              <li>
                <a href="nav-link">Editorial</a>
              </li>
              <li>
                <a href="nav-link">Social Media</a>
              </li>
              <li>
                <a href="nav-link">Support</a>
              </li>
              <li>
                <a href="nav-link">Analysis Report</a>
              </li>
            </ul>
            <ul className="nav navbar-nav navbar-right">
              <li>
                <a href="nav-link">Sign Up</a>
              </li>
              <li>
                <a href="nav-link">Sign In</a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    );
  }
}
