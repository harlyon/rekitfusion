import React, { Component } from 'react';

export default class Sidebar extends Component {
  static propTypes = {};

  render() {
    return (
      <div className="wrapper">
        <nav className="main-menu">
          <br />
          <br />
          <br />
          <br />
          <br />
          <br />
          <ul>
            <li>
              <a href="http://justinfarrow.com">
                <span className="nav-text" />
              </a>
              <br />
            </li>
            <li>
              <a href="http://justinfarrow.com">
                <i className="fa fa-building fa-2x" />
                <span className="nav-text">Spot List</span>
              </a>
              <br />
            </li>
            <li className="has-subnav">
              <a href="#">
                <i className="fa fa-play fa-2x" />
                <span className="nav-text">Media Type</span>
              </a>
              <br />
            </li>
            <li className="has-subnav">
              <a href="#">
                <i className="fa fa-code fa-2x" />
                <span className="nav-text">Source</span>
              </a>
              <br />
            </li>
            <li className="has-subnav">
              <a href="#">
                <i className="fa fa-tag fa-2x" />
                <span className="nav-text">Brand</span>
              </a>
              <br />
            </li>
            <li>
              <a href="#">
                <i className="fa fa-folder-open fa-2x" />
                <span className="nav-text">Advert Type</span>
              </a>
              <br />
            </li>
            <li>
              <a href="#">
                <i className="fa fa-globe fa-2x" />
                <span className="nav-text">Location</span>
              </a>
              <br />
            </li>
            <li>
              <a href="#">
                <i className="fa fa-user-secret fa-2x" />
                <span className="nav-text">Language</span>
              </a>
              <br />
            </li>
            <li>
              <a href="#">
                <i className="fa fa-map fa-2x" />
                <span className="nav-text">Share Voice</span>
              </a>
              <br />
            </li>
            <li>
              <a href="#">
                <i className="fa fa-file fa-2x" />
                <span className="nav-text">Source List</span>
              </a>
              <br />
            </li>
            <li>
              <a href="#">
                <i className="fa fa-user-plus fa-2x" />
                <span className="nav-text">Support</span>
              </a>
              <br />
            </li>
            <li>
              <a href="#">
                <i className="fa fa-volume-up fa-2x" />
                <span className="nav-text">Volume</span>
              </a>
              <br />
            </li>
          </ul>

          <ul className="logout">
            <li>
              <a href="#">
                <i className="fa fa-power-off fa-2x" />
                <span className="nav-text">Log Out</span>
              </a>
            </li>
          </ul>
        </nav>
        <div className="row text-center content">
          <h1>Spot List - Advertisement : Newcomers</h1>
          <br />
          <br />
          <hr />
          <div className="items">
            <div className="col-md-4">
              <a href="#" className="button">
                <h1>
                  0 Spots<h6>from selected filters</h6>
                </h1>
              </a>
            </div>
            <div className="col-md-4">
              
            </div>
            <div className="col-md-4">
              <a href="#" className="button">
                <h1>+ Data Filters</h1>
              </a>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
