import React, { Component } from 'react';

export default class SignIn extends Component {
  static propTypes = {};

  render() {
    return (
      <div className="wrapper">
        <div className="row">
          <div className="login">
            <h2 className="sign">Sign In</h2>
            <input name="username" placeholder="Username" type="text" required />
            <input id="pw" name="password" placeholder="Password" type="password" required />
            <br />
            <br />
            <input class="animated" type="submit" value="Register" />
          </div>
        </div>
      </div>
    );
  }
}
