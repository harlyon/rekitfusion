import React, { Component } from 'react';

export default class WelcomePage extends Component {
  static propTypes = {};
  
  render() {
    return (
      <div className="wrapper">
        <div className="row">
          <div className="padding-tb100 ">
            <div className="col-md-3 col-md-offset-0 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1 text-center margin-tb10 list">
              <i className="fas fa-tv" />
              <h4 className="margin-t10 hero">Advertising</h4>
              <br />
              <p className="margin-t10">
                <div class="dropup">
                  <button className="btn btn-default button dropdown-toggle" type="button" data-toggle="dropdown">
                    Newcomers
                    <span className="caret" />
                  </button>
                  <ul class="dropdown-menu">
                    <li>
                      <a href="#">15 Items last 7days</a>
                    </li>
                    <li>
                      <a href="#">CSS</a>
                    </li>
                    <li>
                      <a href="#">JavaScript</a>
                    </li>
                  </ul>
                  <span class="notify">0%</span>
                </div>
                <a id="add_row" className="btn btn-default button">
                  Allcomers
                </a>
                <span class="notify-1">2%</span>
                <a id="add_row" className="btn btn-default button">
                  MediaTrac
                </a>
                <span class="notify">2%</span>
              </p>
            </div>
            <div className="col-md-3 col-md-offset-0 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1 text-center margin-tb10 list">
              <i className="fas fa-newspaper" />
              <h4 className="margin-t10">Editorial</h4>
              <br />
              <p className="margin-t10">
                <a id="add_row" className="btn btn-default button">
                  Print
                </a>
                <span class="notify">1%</span>
                <a id="add_row" className="btn btn-default button">
                  Broadcast
                </a>
                <span class="notify-1">0%</span>
                <a id="add_row" className="btn btn-default button">
                  Online
                </a>
                <span class="notify">1%</span>
              </p>
            </div>
            <div className="col-md-3 col-md-offset-0 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1 text-center margin-tb10 list">
              <i className="fas fa-mobile" />
              <h4 className="margin-t10">Social Media</h4>
              <br />
              <p className="margin-t10">
                <a id="add_row" className="btn btn-default button">
                  Login
                </a>
              </p>
            </div>
            <div className="col-md-3 col-md-offset-0 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1 text-center margin-tb10 list">
              <i className="fas fa-tv" />
              <h4 className="margin-t10">Analysis Report</h4>
              <br />
              <p className="margin-t10">
                <a id="add_row" className="btn btn-default button">
                  Reports
                </a>
                <span class="notify">1%</span>
              </p>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
