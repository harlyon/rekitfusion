import React, { Component } from 'react';
import PropTypes from 'prop-types';
import SimpleNav from '../common/SimpleNav';
import routeConfig from '../../common/routeConfig';
import Loadingscreen from '../common/Loadingscreen';
import WelcomePage from '../common/WelcomePage';
import CreateAccount from '../common/CreateAccount';
import SignIn from '../common/SignIn';
import Sidebar from '../common/Sidebar';
import LandingPage from '../common/LandingPage';
import Navbar from '../common/Navbar';


/*
  This is the root component of your app. Here you define the overall layout
  and the container of the react router. The default one is a two columns layout.
  You should adjust it according to the requirement of your app.
*/
export default class App extends Component {
  static propTypes = {
    children: PropTypes.node,
  };

  static defaultProps = {
    children: 'No content.',
  };

  render() {
    return (
      <div className="home-app">
        <WelcomePage />
      </div>
    );
  }
}
