import React from 'react';
import { shallow } from 'enzyme';
import { expect } from 'chai';
import { CreateAccount } from 'src/features/common';

describe('common/CreateAccount', () => {
  it('renders node with correct class name', () => {
    const renderedComponent = shallow(
      <CreateAccount />
    );

    expect(
      renderedComponent.find('.common-create-account').getElement()
    ).to.exist;
  });
});
