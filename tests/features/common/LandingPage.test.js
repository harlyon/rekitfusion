import React from 'react';
import { shallow } from 'enzyme';
import { expect } from 'chai';
import { LandingPage } from 'src/features/common';

describe('common/LandingPage', () => {
  it('renders node with correct class name', () => {
    const renderedComponent = shallow(
      <LandingPage />
    );

    expect(
      renderedComponent.find('.common-landing-page').getElement()
    ).to.exist;
  });
});
