import React from 'react';
import { shallow } from 'enzyme';
import { expect } from 'chai';
import { Loadingscreen } from 'src/features/common';

describe('common/Loadingscreen', () => {
  it('renders node with correct class name', () => {
    const renderedComponent = shallow(
      <Loadingscreen />
    );

    expect(
      renderedComponent.find('.common-loadingscreen').getElement()
    ).to.exist;
  });
});
