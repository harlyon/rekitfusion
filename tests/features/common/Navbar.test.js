import React from 'react';
import { shallow } from 'enzyme';
import { expect } from 'chai';
import { Navbar } from 'src/features/common';

describe('common/Navbar', () => {
  it('renders node with correct class name', () => {
    const renderedComponent = shallow(
      <Navbar />
    );

    expect(
      renderedComponent.find('.common-navbar').getElement()
    ).to.exist;
  });
});
