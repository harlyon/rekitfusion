import React from 'react';
import { shallow } from 'enzyme';
import { expect } from 'chai';
import { SignIn } from 'src/features/common';

describe('common/SignIn', () => {
  it('renders node with correct class name', () => {
    const renderedComponent = shallow(
      <SignIn />
    );

    expect(
      renderedComponent.find('.common-sign-in').getElement()
    ).to.exist;
  });
});
